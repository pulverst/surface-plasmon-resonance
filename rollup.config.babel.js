import path from 'path'
import babel from 'rollup-plugin-babel'
import resolve from 'rollup-plugin-node-resolve'
import commonjs from 'rollup-plugin-commonjs'
import postcss from 'rollup-plugin-postcss'

const genericOutputConfig = {
  name: `${process.env.npm_package_name}-components`,
  sourcemap: process.env.NODE_ENV !== 'production',
  // don't include those libraries into bundle
  external: ['react', 'react-dom'],
  globals: {
    react: 'React',
    'react-dom': 'ReactDOM',
  }
}

export default {
  input: './src/components/export.jsx',

  output: [{
    ...genericOutputConfig,
    format: 'cjs',
    file: './build-components/components.js',
  },
  {
    ...genericOutputConfig,
    format: 'es',
    file: './build-components/components.module.js',
  }],

  plugins: [
    postcss({
      modules: true,
    }),
    babel({
      exclude: 'node_modules/**'
    }),
    resolve({
      // support .jsx enxtension
      extensions: [ '.mjs', '.js', '.jsx', '.json', '.node' ], 
    }),
    commonjs()
  ],
}