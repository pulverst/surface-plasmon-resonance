import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { LOGIN } from '../paths';

import { applicationUserIsAuthenticatedSelector } from '../redux/application/selectors';

const PrivateRoute = ({ component: Component, isAuthenticated, ...rest }) => (
  <Route
    {...rest}
    render={props => isAuthenticated ? (
        <Component {...props} />
      ) : (
        <Redirect
          to={{
            pathname: LOGIN,
            state: { from: props.location }
          }}
        />
      )
    } 
  />
)

const select = state => ({
  isAuthenticated: applicationUserIsAuthenticatedSelector(state),
})

const mapDispatchToProps = ({});

export default connect(select, mapDispatchToProps)(PrivateRoute);
