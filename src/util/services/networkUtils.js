import networkEndpoints from './network_endpoints.json';
import { createNetworkError, createInvalidDataError } from '../ErrorHandler';

export const defaultConfig = {
  method: 'GET',
  cors: true,
  credentials: 'include',
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};

const getEnv = () => {
  const url = window.location.href.toLowerCase()
  // check for localhost / 127.0.0.1
  if (url.indexOf('localhost') || url.indexOf('127.0.0.1')) {
    return 'local'
  }
  return process.env.NODE_ENV;
};

export const getUrl = key => networkEndpoints[key][getEnv()]

// generic call definitions
export const genericFetch = (url, config = defaultConfig) => {
  return fetch(url, config)
    .catch(error => Promise.reject(createNetworkError(error, undefined)))
    .then((response) => {
      const { ok, status } = response;
      if (!ok || status >= 300 || status < 200) {
        // invalid
        console.log(response)
        const message = `Network error detected, status code: ${status}`;
        const error = createNetworkError(new Error(message), response);
        return Promise.reject(error);
      }

      return Promise.resolve(response);
    });
};

export const genericJsonFetch = (url, config) =>
  genericFetch(url, config)
    // stop if generic fetch fails
    .catch(error => Promise.reject(error))
    // get json response
    .then((response) => {
      return response.json()
        // handle invalid json payload
        .catch(error => Promise.reject(createInvalidDataError(error, response)));
    });
  
export const dataStoreUploadRequest = (url, config = {}, body = {}) => {
  const dcRegisterBody = {
    igmClassification: 'BUSINESS_USE_ONLY',
    department: 'NIBR_IT/IS/DELTA',
    source: 'NIBR',
    privacyCategory: 'NOT_APPLICABLE',
    domain: 'INTERNAL',
    ...body,
  };

  const newConfig = {
    ...defaultConfig,
    method: 'POST',
    ...config,
    body: JSON.stringify(dcRegisterBody), // body is hardcoded by dcRegisterBody object
  };

  return genericJsonFetch(url, newConfig);
};

export const pharonRequest = (url, row = {}, command) => {
  const pharonUploadBody = {
    "header": {
      "dataStore": "BioBook 9.1.1.2",
      "generator": "Interface BioBook-Pharon",
      "instance": "caidbsppoc",
      "command": command,
      "loaderVersion": "1.1"
    },
    "definition": {
      "assayVersionId": "500000117679",
      "assayName": "SPR Assay",
      "versionMajor": "1",
      "versionMinor": "0",
      "operator": "PULVERST",
      "expId": "467"
    },
    "row": [
      row
    ]
  };

  //console.log(JSON.stringify(pharonUploadBody))

  const newConfig = {
    method: 'POST',
    credentials: 'include',  
    headers: {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(pharonUploadBody),
  };

  return genericJsonFetch(url, newConfig)
};

export default {
  getUrl,
  genericFetch,
  genericJsonFetch,
};
