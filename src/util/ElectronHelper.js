class ElectronHelper {
  ipcRenderer = null;

  constructor(ipcRenderer = null) {
    if (!ipcRenderer) {
      throw new Error('icpRenderer must not be null');
    }
    this.ipcRenderer = ipcRenderer;
  }

  sendAsync(eventData, actionData, timeoutTimer = 5000) {
    if (!this.ipcRenderer) {
      throw new Error('icpRender is not defined');
    }

    if (!eventData) {
      throw new Error('event is not defined');
    }

    // return promise to allow async calling
    return new Promise((resolve, reject) => {

      // set timemout if timeout time is set
      let timeout;
      if (timeoutTimer) {
        timeout = setTimeout(() => {
          reject('timed out');
        }, timeoutTimer);
      }

      console.log('event: [' + eventData + '], action: ', actionData, 'timeout: [' + timeoutTimer + ']');
      this.ipcRenderer.send(eventData, actionData);

      // adds a one time listener
      this.ipcRenderer.once(eventData, (event, action) => {
        const { payload, error } = action;
        
        // kill timeout timer, if initialized
        if (timeoutTimer) {
          clearTimeout(timeout);
        }

        // check for error
        if (error) {
          reject(payload);
        } else {
          resolve(payload);
        }
      });
    });
  }

  getIpcRenderer() {
    return this.ipcRenderer;
  }
}

export default ElectronHelper;