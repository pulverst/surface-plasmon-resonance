// index of files to upload
const fileIndex = [
  6,   // Curve 1: Sensorgram
  7,   // Curve 2: KD at Equilibrium plot
  8,   // Curve 3: Kinetic Fit plot
];

// index of value data
const valueIndex = [
  0,   // Project Code
  1,   // Target
  2,   // value ID
  3,   // Source
  4,   // SPR Method
  5,   // Summary
  6,   // Curve ID 1 (Sensorgram)
  7,   // Curve ID 2 (KD at Equilibrium plot)
  8,   // Curve ID 3 (Kinetic Fit plot)
  9,   // Affinity KD
  10,  // Kinetic KD
  11,  // Comment
  12,  // kon
  13,  // koff
  14,  // Max Conc
  15,  // Theoretical Rmax
  16,  // % Rmax
  17,  // Plate ID
  18,  // Running Buffer
  19,  // Instrument
  20,  // Assay conditions
  21,  // iProt Number
  22,  // Target Description
  23,  // Target Comment
  24,  // Lab Date
  25,  // Operator
  26,  // Genedata DB ID
  27,  // ELN
  28,  // ARES ID
];

// index of meta data
const metaIndex = [
  0,   // Project code
  1,   // Target
  3,   // Source
  4,   // SPR Method
];

module.exports = {fileIndex, valueIndex, metaIndex};
exports = {fileIndex} ;
