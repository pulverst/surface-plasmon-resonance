import { createSelector } from 'reselect';

import { INITIAL, VALIDATED, READY, RETRY, DONE, FAILURE, PENDING, UPLOADED, SUCCESS } from '../../util/RequestStatus';

const compoundStateSelector = state => state.compounds;

export const compoundIdsSelector = createSelector(
  compoundStateSelector,
  ({ ids }) => ids,
);

export const compoundByIdSelector = createSelector(
  compoundStateSelector,
  ({ byId }) => byId,
);

export const compoundRequestSelector = createSelector(
  compoundStateSelector,
  ({ requestCompoundStatus }) => requestCompoundStatus,
);

export const compoundUploadSelector = createSelector(
  compoundStateSelector,
  ({ requestCompoundUpload }) => requestCompoundUpload,
);

export const compoundListSelector = createSelector(
  compoundIdsSelector,
  compoundByIdSelector,
  (ids, byId) => {
    if (ids.length === null) {
      return [];
    }
    return ids.map(id => byId[id])
      .filter(item => item !== null);
  },
);

export const compoundValidateDoneSelector = createSelector(
  compoundIdsSelector,
  compoundByIdSelector,
  (ids, byId) => {
    if (ids.length === null) {
      return [];
    }
    return ids.map(id => byId[id])
      .filter(item => item.status === VALIDATED);
  },
);

export const compoundUploadRetrySelector = createSelector(
  compoundIdsSelector,
  compoundByIdSelector,
  (ids, byId) => {
    if (ids.length === null) {
      return [];
    }
    return ids.map(id => byId[id])
      .filter(item => item.status === RETRY)
  },
);

export const compoundUploadDoneSelector = createSelector(
  compoundIdsSelector,
  compoundByIdSelector,
  (ids, byId) => {
    if (ids.length === null) {
      return [];
    }
    return ids.map(id => byId[id])
      .filter(item => item.status === DONE);
  },
);

export const compoundUploadFailedSelector = createSelector(
  compoundIdsSelector,
  compoundByIdSelector,
  (ids, byId) => {
    if (ids.length === null) {
      return [];
    }
    return ids.map(id => byId[id])
      .filter(item => item.status === FAILURE)
  },
);

export const compoundStatusSelector = createSelector(
  compoundIdsSelector,
  compoundByIdSelector,
  (ids, byId) => {
    if (ids.length === null) {
      return [];
    }
    const requests = ids.map(id => byId[id].status)
    if (requests.filter(item => item === FAILURE).length > 0) {
      return FAILURE;
    }
    if (requests.filter(item => item === RETRY).length > 0) {
      return RETRY;
    }
    if (requests.filter(item => item === PENDING).length > 0) {
      return PENDING;
    }
    if (requests.filter(item => item === VALIDATED).length > 0) {
      return VALIDATED;
    }
    if (requests.filter(item => item === READY).length > 0) {
      return READY;
    }
    if (requests.filter(item => item === INITIAL).length > 0) {
      return INITIAL;
    }
    if (requests.filter(item => item === SUCCESS).length > 0) {
      return SUCCESS;
    }
    if (requests.filter(item => item === UPLOADED).length > 0) {
      return UPLOADED;
    }
    return INITIAL;
  },
);
