import { getUrl, pharonRequest, genericFetch, genericJsonFetch } from '../../util/services/networkUtils';
import * as mapping from './mapping';

export const healthCheck = () => {
  const pharonHealthCheckUrl = getUrl('pharonHealthCheck');
  const config = { 
    method: 'GET',
    credentials: 'include', 
  };
  return genericFetch(pharonHealthCheckUrl, config);
};

export const pharonAssayItems = (assayID) => {
  const pharonItemsUrl = getUrl('pharon') + '/' + assayID + '/items';
  const config = {
    mehtod: 'GET',
    credentials: 'include',
  };
  return genericJsonFetch(pharonItemsUrl, config);
};

const itemValue = (files, key, value, fileHeaders) => {

  // replace '-' with null for Pharon upload
  const cleanValue = value === '-' ? null : value;

  // replace curve identifier
  if (fileHeaders.filter((header) => header === key).length > 0)
  {
    return cleanValue ? files[value].identifier : cleanValue;
  }
  return cleanValue;
}

const doPharonRequest = (compound, files, itemDefinition, assayID, fileHeaders, command) => {

  const items = (compound) => {
    const items = [];
    Object.keys(compound).forEach(function(key,index) {
      if (itemDefinition[mapping.pharonHeaderMapping[key]] !== undefined) {
        items.push ({
          "itemId": itemDefinition[mapping.pharonHeaderMapping[key]].itemId,
          "qualifier": "",
          "value": itemValue(files, key, compound[key], fileHeaders),
        });
      }
    });
    return items;
  };

  const itemBody = {
    "compound": compound["Compound ID"],
    "item" : items(compound) 
  };

  const pharonUrl = `${getUrl('pharon')}/${assayID}/${command === 'insert' ? 'results' : 'validate'}`;
  return pharonRequest(pharonUrl, itemBody, command)
    .catch((error) => Promise.reject({
      error,
      type: 'pharon ' + command,
    }))
    .then(response => {
      //console.log('Pharon ' + command + ' done.');
    });
};

export const uploadCompound = (compound, files, itemDefinition, assayID, fileHeaders) => {
  return doPharonRequest(compound, files, itemDefinition, assayID, fileHeaders, 'insert');
};

export const validateCompound = (compound, files, itemDefinition, assayID, fileHeaders) => {
  return doPharonRequest(compound, files, itemDefinition, assayID, fileHeaders, 'validate');
};

