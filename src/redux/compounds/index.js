import { combineReducers } from 'redux';

import { PENDING, VALIDATED, FAILURE, RETRY, UPLOADED } from '../../util/RequestStatus';
import { fileIndex } from '../../util/ParserInfo';

import * as API from './api';
import { uploadFileRequest } from '../files/api';

import {
  fileUploadRequest,
  fileUploadSuccess,
  fileUploadFailure,
  fileUploadRetry,
  filesStatusRequest,
  filesStatusSuccess,
  filesStatusFailure
} from '../files';

import { fileByIdSelector } from '../files/selector';

import { metaSetValues } from '../meta';
// import { metaHeadersSelectors } from '../meta/selector';


// action types
const COMPOUND_STATUS_REQUEST = "COMPOUND_STATUS_REQUEST";
const COMPOUND_STATUS_SUCCESS = "COMPOUND_STATUS_SUCCESS";
const COMPOUND_STATUS_FAILURE = "COMPOUND_STATUS_FAILURE";

const COMPOUND_VALIDATE_REQUEST = "COMPOUND_VALIDATE_REQUEST";
const COMPOUND_VALIDATE_SUCCESS = "COMPOUND_VALIDATE_SUCCESS";
const COMPOUND_VALIDATE_FAILURE = "COMPOUND_VALIDATE_FAILURE";

const COMPOUND_UPLOAD_REQUEST = "COMPOUND_UPLOAD_REQUEST";
const COMPOUND_UPLOAD_SUCCESS = "COMPOUND_UPLOAD_SUCCESS";
const COMPOUND_UPLOAD_FAILURE = "COMPOUND_UPLOAD_FAILURE";
const COMPOUND_UPLOAD_RETRY   = "COMPOUND_UPLOAD_RETRY";

// const COMPOUND_SET_VALUE = 'COMPOUND_SET_VALUE';
// const COMPOUND_SET_VALUES = 'COMPOUND_SET_VALUES';
// const COMPOUND_CLEAR_VALUE = 'COMPOUND_CLEAR_VALUE';

const MAX_RETRY = 3;
const DELAY = 5000;
const TIMEOUT_STRETCH = 1.2;

const assayID = '500000117679';

// ---- simple actions---------------------------------------------------------

// compound status actions
export const compoundStatusRequest = () => ({ type: COMPOUND_STATUS_REQUEST });

export const compoundStatusSuccess = (compounds) => {
  const ids = [];
  const byId = {};

  compounds.forEach((compound) => {
    const { id } = compound;
    
    ids.push(id)
    byId[id] = compound;
  });

  return {
    type: COMPOUND_STATUS_SUCCESS,
    payload: {
      byId,
      ids,
    },
  };
};

export const compoundStatusFailure = (error) => ({
  type: COMPOUND_STATUS_FAILURE,
  payload: error,
  error: true,
});

// compound validate actions
export const compoundValidateRequest = (compoundId) => ({
  type: COMPOUND_VALIDATE_REQUEST,
  payload: {
    compoundId,
  },
});

export const compoundValidateSuccess = (compoundId, validated) => {
  return {
    type: COMPOUND_VALIDATE_SUCCESS,
    payload: {
      compoundId,
      validated,
    },
  };
};

export const compoundValidateFailure = (compoundId, error) => ({
  type: COMPOUND_VALIDATE_FAILURE,
  payload: {
    error: error,
    compoundId,
  }
});

// compound upload actions
export const compoundUploadRequest = (compoundId) => ({
  type: COMPOUND_UPLOAD_REQUEST,
  payload: {
    compoundId,
  },
});

export const compoundUploadSuccess = (compoundId, identifier) => {
  return {
    type: COMPOUND_UPLOAD_SUCCESS,
    payload: {
      compoundId,
      identifier,
    },
  };
};

export const compoundUploadFailure = (compoundId, error) => ({
  type: COMPOUND_UPLOAD_FAILURE,
  payload: {
    error: error,
    compoundId,
  }
});

export const compoundUploadRetry = (compoundId, retry) => ({
  type: COMPOUND_UPLOAD_RETRY,
  payload: {
    compoundId,
    retry
  },
});

// const compoundSetValue = (key, value) => ({
//   type: COMPOUND_SET_VALUE,
//   payload: {
//     key,
//     value,
//   },
// })

// export const compoundSetValues = (obj) => ({
//   type: COMPOUND_SET_VALUES,
//   payload: obj,
// })

// const compoundClearValue = key => ({
//   type: COMPOUND_CLEAR_VALUE,
//   payload: key,
// })


// ---- complex actions--------------------------------------------------------

// Load export summary file
const loadExportData = filename => (dispatch, getState, obj) => {
  
  const { ElectronHelper } = obj;

  dispatch(filesStatusRequest());
  dispatch(compoundStatusRequest());

  ElectronHelper.sendAsync('load-spr-export', { filename }, 0)
    .then(({ meta, compounds, files }) => {
      dispatch(filesStatusSuccess(files));
      dispatch(compoundStatusSuccess(compounds));
      dispatch(metaSetValues(meta));  
      dispatch(validateCompounds(compounds));
    })
    .catch((error) => {
      dispatch(filesStatusFailure(error));
      dispatch(compoundStatusFailure(error));
    });
};

// Validate compounds
const validateCompounds = (compounds) => (dispatch, getState) => {

  //set status
  compounds.forEach((compound) => {
    dispatch(compoundValidateRequest(compound.id));
  });

  const state = getState();
  const filesById = fileByIdSelector(state);
  
  const fileHeaders = fileIndex.map((index) => {
    return state.meta.headers[index];
  });

  // get assay items
  return API.pharonAssayItems(assayID)
  .catch((error) => Promise.reject({
    error,
    type: 'mapping',
  }))
  .then((items) => {

    const arrayToObject = (array, keyField) => array.reduce((obj, item) => {
      obj[item[keyField]] = item;
      return obj;
    }, {});

    const itemDefinition = arrayToObject(items.assayItems, "item");

    // create promises
    const promises = compounds.map((compoundObject) => {
      return API.validateCompound(compoundObject, filesById, itemDefinition, assayID, fileHeaders)
      .then((imgUrl) => {
        dispatch(compoundValidateSuccess(compoundObject.id, imgUrl));
      })
      .catch((error ) => {
        dispatch(compoundValidateFailure(compoundObject.id, error));
        return Promise.reject(error);
      });
    });

    Promise.all(promises)
      .then(() => {
        console.log('All compounds validated');
      })
      .catch((error) => {
        console.log('Compound validation error:', error);
      });
    }
    );
};

// get item definition from pharon items
const arrayToObject = (array, keyField) => array.reduce((obj, item) => {
  obj[item[keyField]] = item;
  return obj;
}, {});

const uploadCompounds = (compounds) => async (dispatch, getState) => {
  try {
    // get pharon item definition
    const items = await API.pharonAssayItems(assayID);
    const itemDefinition = arrayToObject(items.assayItems, "item");

    // upload compounds
    compounds.forEach((compound) => {
      dispatch(uploadCompound(compound, itemDefinition));
    });

  } catch (error) {
    // handle error
    console.log(error);
  }
};

const uploadCompound = (compound, itemDefinition) => async (dispatch, getState, { ElectronHelper }) => {
  try {
    const state = getState();

    const { files: fileIds } = compound;
    const fileById = fileByIdSelector(state);

    let delay = DELAY;

    dispatch(compoundUploadRequest(compound.id, ''));

    const imgUrls = await Promise.all(fileIds.map((fileId) => {
      // set file upload state
      dispatch(fileUploadRequest(fileId));

      // get file
      const fileObject = fileById[fileId];

      // start upload
      return ElectronHelper.sendAsync('read-spr-file', { fileObject }, 0)
        .catch((error) => Promise.reject({
          error,
          type: 'access',
        }))
        .then((fileData) => {
          // upload jpg file
          const fileInformation = {
            fileData,
            fileObject,
          };
          return uploadFileRequest(fileInformation);
        })
        .then(({ imgUrl }) => {
          // update redux file state
          dispatch(fileUploadSuccess(fileObject.id, imgUrl));

          // return imgUrls by ID
          return {
            id: fileObject.id,
            imgUrl,
          };
        })
        // handle retry
        .catch(({ error }) => {
          if (fileObject.retry && fileObject.retry >= MAX_RETRY) {
            dispatch(fileUploadFailure(fileObject.id, error));
          }
          else {
            const nextRetry = fileObject.retry === undefined ? 1 : fileObject.retry + 1;
            console.log('next retry:', nextRetry, ' delay:', delay);
            setTimeout(dispatch, delay, fileUploadRetry(fileObject.id, nextRetry));
            delay *= TIMEOUT_STRETCH;
          }
          return Promise.reject(error);
        });
      })
    );

    //console.log(imgUrls);

    const fileHeaders = fileIndex.map((index) => {
      return state.meta.headers[index];
    });

    // upload compound to Pharon
    const uploadedCompound = await API.uploadCompound(compound, fileById, itemDefinition, assayID, fileHeaders);

    dispatch(compoundUploadSuccess(compound.id, ''));

  } catch (error) {
    // handle compound upload error
    console.log(error);
    dispatch(compoundUploadFailure(compound.id));
  }
};

// ----------------------------------------------------------------------------
// reducers

const requestInitialState = {
  lastSuccess: null,
  lastError: null,
  error: null,
  loading: false,
};

const requestCompoundStatus = (state = requestInitialState, action) => {
  switch (action.type) {
    case COMPOUND_STATUS_REQUEST:
      return {
        ...requestInitialState,
        loading: true,
      };
    case COMPOUND_STATUS_SUCCESS:
      return {
        ...state,
        loading: false,
        lastSuccess: Date.now(),
      };
    case COMPOUND_STATUS_FAILURE:
      return {
        ...state,
        loading: false,
        lastError: Date.now(),
        error: action.payload.error,
      };
    default:
      return state;
  }
};

const requestCompoundValidate = (state = requestInitialState, action) => {
  switch (action.type) {
    case COMPOUND_VALIDATE_REQUEST:
      return {
        ...requestInitialState,
        loading: true,
      };
    case COMPOUND_VALIDATE_SUCCESS:
      return {
        ...state,
        loading: false,
        lastSuccess: Date.now(),
      };
    case COMPOUND_VALIDATE_FAILURE:
      return {
        ...state,
        loading: false,
        lastError: Date.now(),
        error: action.payload.error,
      };
    default:
      return state;
  }
};

const requestCompoundUpload = (state = requestInitialState, action) => {
  switch (action.type) {
    case COMPOUND_UPLOAD_REQUEST:
      return {
        ...requestInitialState,
        loading: true,
      };
    case COMPOUND_UPLOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        lastSuccess: Date.now(),
      };
    case COMPOUND_UPLOAD_FAILURE:
      return {
        ...state,
        loading: false,
        lastError: Date.now(),
        error: action.payload.error,
      };
    default:
      return state;
  }
};

//-------------------------------------------------------------------------------------------------
const byId = (state = {}, action) => {
  switch (action.type) {
    case COMPOUND_VALIDATE_REQUEST:
      return {
        ...state,
        [action.payload.compoundId]: {
          ...state[action.payload.compoundId],
          status: PENDING,
        },
      };
    case COMPOUND_VALIDATE_SUCCESS:
      return {
        ...state,
        [action.payload.compoundId]: {
          ...state[action.payload.compoundId],
          status: VALIDATED,
        },
      };
    case COMPOUND_VALIDATE_FAILURE:
      return {
        ...state,
        [action.payload.compoundId]: {
          ...state[action.payload.compoundId],
          status: FAILURE,
        },
      };
    case COMPOUND_UPLOAD_REQUEST:
      return {
        ...state,
        [action.payload.compoundId]: {
          ...state[action.payload.compoundId],
          status: PENDING,
        },
      };
    case COMPOUND_UPLOAD_SUCCESS:
      return {
        ...state,
        [action.payload.compoundId]: {
          ...state[action.payload.compoundId],
          status: UPLOADED,
        },
      };
    case COMPOUND_UPLOAD_FAILURE:
      return {
        ...state,
        [action.payload.compoundId]: {
          ...state[action.payload.compoundId],
          status: FAILURE,
        },
      };
    case COMPOUND_UPLOAD_RETRY:
      return {
        ...state,
        [action.payload.compoundId]: {
          ...state[action.payload.compoundId],
          status: RETRY,
          retry: action.payload.retry,
        }
      };
    case COMPOUND_STATUS_SUCCESS:
      return {
        ...state,
        ...action.payload.byId,
      };
    default:
      return state;
  }
};

const ids = (state = [], action) => {
  switch (action.type) {
    case COMPOUND_STATUS_SUCCESS:
      return [...new Set([...state, ...action.payload.ids])]
    default:
      return state;
  }
};

// export (state = {}, action) => {
//   const { payload } = action;
//   switch (action.type) {
//     case COMPOUND_SET_VALUE:
//       return {
//         ...state,
//         [payload.key]: [payload.value],
//       }
//     case COMPOUND_SET_VALUES:
//       return {
//         ...state,
//         ...payload,
//       }
//     case COMPOUND_CLEAR_VALUE:
//       return {
//         ...state,
//         [payload]: undefined,
//       }
//     default:
//       return state;
//   }
// };

export {
  loadExportData,
  uploadCompounds,
  validateCompounds,
};

const compoundsReducer = combineReducers({
  requestCompoundStatus,
  requestCompoundValidate,
  requestCompoundUpload,
  byId,
  ids,
});

export default (state, action) => {
  if (action.type === COMPOUND_STATUS_REQUEST) {
    state = undefined;
  }
  return compoundsReducer(state, action);
}
