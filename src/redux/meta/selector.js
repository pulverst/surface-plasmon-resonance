import { createSelector } from 'reselect';

export const metaStateSelector = state => state.meta;

export const metaSummarySelector = createSelector(
  metaStateSelector,
  ({ Summary }) => Summary,
);

export const metaHeadersSelectors = createSelector(
  metaStateSelector,
  ({ headers }) => headers || {}
);
