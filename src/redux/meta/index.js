// key definitions
export const KEY_FILE_NAME = 'KEY_FILE_NAME';

// actions
const META_SET_VALUE = 'META_SET_VALUE';
const META_SET_VALUES = 'META_SET_VALUES';
const META_CLEAR_VALUE = 'META_CLEAR_VALUE';

// simple actions
const metaSetValue = (key, value) => ({
  type: META_SET_VALUE,
  payload: {
    key,
    value,
  },
});

export const metaSetValues = (obj) => ({
  type: META_SET_VALUES,
  payload: obj,
});

const metaClearValue = key => ({
  type: META_CLEAR_VALUE,
  payload: key,
});


// complex actions

// reducers
export default (state = {}, action) => {
  const { payload } = action;
  switch (action.type) {
    case META_SET_VALUE:
      return {
        ...state,
        [payload.key]: [payload.value],
      }
    case META_SET_VALUES:
      return {
        ...state,
        ...payload,
      }
    case META_CLEAR_VALUE:
      return {
        ...state,
        [payload]: undefined,
      }
    default:
      return state;
  }
};
