import { createStore, applyMiddleware, compose } from 'redux';
import thunkMiddleware from 'redux-thunk';
import analytics from 'nxc-usage-analytics';
import { debounce } from 'lodash';
import ric from 'ric-shim';

import { createMiddleware } from '../util/ErrorHandler';
import rootReducer from './rootReducer';

import configureReactors from './reactors/configureReactors';
import { APP_IDLE } from './application';

import { addUnhandledPromiseCatcher } from '../util/ErrorHandler';

import ElectronHelper from '../util/ElectronHelper';
const { ipcRenderer } = window.require('electron');

const configureStore = (initialState = {}) => {
  const {
    NODE_ENV,
    REACT_APP_NAME,
    REACT_APP_WEBTRENDS_ID,
    REACT_APP_APPSTORE_ID,
  } = process.env;

  const logToUI = (error) => {
    console.log('Send to server', error);
    // fetch();
  }

  const errorMiddleware = createMiddleware({ logToUI });

  const enhancers = [];
  let middleware = [thunkMiddleware.withExtraArgument({ ElectronHelper: new ElectronHelper(ipcRenderer) }), errorMiddleware];

  if (REACT_APP_WEBTRENDS_ID && REACT_APP_APPSTORE_ID && NODE_ENV === 'production') {
    // add usage analytics on prod, if token is provided
    middleware.push(
      analytics({
        dcsId: REACT_APP_WEBTRENDS_ID,
        appName: REACT_APP_NAME,
        appStoreId: REACT_APP_APPSTORE_ID,
      })
    );
  }

  let composeEnhancers = compose;
  if (NODE_ENV === 'development') {
    const devToolsExtension = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__;

    if (typeof devToolsExtension === 'function') {
      composeEnhancers = devToolsExtension;
    }
  }

  const store = createStore(
    rootReducer,
    initialState,
    composeEnhancers(
      applyMiddleware(...middleware),
      ...enhancers
    )
  );

  // add reactors
  store.subscribe(configureReactors(store));

  // idle configuration
  const idleDispatcher = () => {
    store.dispatch({ type: APP_IDLE });
  };

  // debounce app idle all 30 seconds
  const deBounced = debounce(() => {
    // The requestAnimationFrame ensures it doesn't run when tab isn't active
    // the requestIdleCallback makes sure the browser isn't busy with something
    // else.
    requestAnimationFrame(() => 
      // this timeout option for requestIdleCallback is a maximum amount of time
      // to wait. I'm including it here since there have been a few browser bugs where
      // for various reasons browsers fail to trigger idle callbacks without this argument.
      ric(idleDispatcher, { timeout: 500 })
    );
  }, 30000);

  // Now this will run *each time* something
  // is dispatched. But once it's been 30 seconds
  // since something has happened. It will cause
  // its *own* dispatch. Which then start the cycle
  // over again.
  store.subscribe(deBounced);

  // add error handler
  addUnhandledPromiseCatcher(store);

  return store;
}

export default configureStore;
