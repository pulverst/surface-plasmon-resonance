import { getUrl, dataStoreUploadRequest, genericFetch } from '../../util/services/networkUtils';

export const uploadFileRequest = ({ fileData, fileObject }) => {
  const { filename } = fileObject;
  // const uploadIdentifier = `${Date.now()}-${filename}`;
  const uploadIdentifier = filename;

  const registerBody = {
    name: uploadIdentifier,
    type: 'FILE',
    typeAttributes: {
      fileName: filename,
    },
    format: 'IMG',
  };

  // register upload at the DataStore and get upload URL
  const registerUrl = getUrl('dataCatalog');
  return dataStoreUploadRequest(registerUrl, {}, registerBody)
    .catch((error) => Promise.reject({
      error,
      type: 'register',
    }))
    .then((dataStoreResponse) => {
      // get upload url
      const { typeAttributes, identifier } = dataStoreResponse;
      const { credentials: { presignedUrl } } = typeAttributes;

      const uploadConfig = {
        method: 'PUT',
        cors: true,
        credentials: 'include',
        headers: {
          'Access-Control-Allow-Origin':'*',
          'Content-Type': 'image/jpeg',
          'x-amz-server-side-encryption': 'AES256',
        },
        body: fileData,
      };

      const imgUrl = registerUrl+identifier;
      return genericFetch(presignedUrl, uploadConfig)
        .catch((error) => Promise.reject({
          error,
          type: 'upload',
        }))
        .then(response => {
          return {
            imgUrl,
          };
        });
    });
};