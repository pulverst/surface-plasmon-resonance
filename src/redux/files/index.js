import { combineReducers } from 'redux';

import * as RequestStatus from '../../util/RequestStatus';

import { uploadFileRequest } from './api';
import { fileListSelector } from './selector';

// action types
const FILES_STATUS_REQUEST = "FILES_STATUS_REQUEST";
const FILES_STATUS_SUCCESS = "FILES_STATUS_SUCCESS";
const FILES_STATUS_FAILURE = "FILES_STATUS_FAILURE";

const FILE_UPLOAD_REQUEST = 'FILE_UPLOAD_REQUEST';
const FILE_UPLOAD_SUCCESS = 'FILE_UPLOAD_SUCCESS';
const FILE_UPLOAD_FAILURE = 'FILE_UPLOAD_FAILURE';
const FILE_UPLOAD_RETRY   = 'FILE_UPLOAD_RETRY';

// const FILE_SET_VALUE = 'FILE_SET_VALUE';
// const FILE_SET_VALUES = 'FILE_SET_VALUES';
// const FILE_CLEAR_VALUE = 'FILE_CLEAR_VALUE';

const MAX_RETRY = 3;
const DELAY = 5000;
const TIMEOUT_STRETCH = 1.2;

// ---- simple actions---------------------------------------------------------

// file status actions
const filesStatusRequest = () => ({ type: FILES_STATUS_REQUEST });

const filesStatusSuccess = (files) => {
  const ids = [];
  const byId = {};

  files.forEach((file) => {
    const { id } = file;
    
    ids.push(id)
    byId[id] = file;
  });

  return {
    type: FILES_STATUS_SUCCESS,
    payload: {
      byId,
      ids,
    },
  };
};

const filesStatusFailure = (error) => ({
  type: FILES_STATUS_FAILURE,
  payload: error,
  error: true,
});

// file upload actions
export const fileUploadRequest = (fileId) => ({
  type: FILE_UPLOAD_REQUEST,
  payload: {
    fileId,
  },
});

export const fileUploadSuccess = (fileId, identifier) => {
  return {
    type: FILE_UPLOAD_SUCCESS,
    payload: {
      fileId,
      identifier,
    },
  };
};

export const fileUploadFailure = (fileId, error) => ({
  type: FILE_UPLOAD_FAILURE,
  payload: {
    error: error,
    fileId: fileId,
  }
})

export const fileUploadRetry = (fileId, retry) => {
  return {
    type: FILE_UPLOAD_RETRY,
    payload: {
      fileId,
      retry,
    }
  };
};

// const fileSetValue = (key, value) => ({
//   type: FILE_SET_VALUE,
//   payload: {
//     key,
//     value,
//   },
// })

// export const fileSetValues = (obj) => ({
//   type: FILE_SET_VALUES,
//   payload: obj,
// })

// const fileClearValue = key => ({
//   type: FILE_CLEAR_VALUE,
//   payload: key,
// })

// ---- complex actions--------------------------------------------------------

// Upload single file
const uploadFile = (fileObject, dispatch, ElectronHelper) => {
  const { id, retry } = fileObject;

  // update pending status
  dispatch(fileUploadRequest(id));

  return ElectronHelper.sendAsync('read-spr-file', { fileObject }, 0)
  .catch((error) => Promise.reject({
    error,
    type: 'access',
  }))
  .then((fileData) => {
    // upload jpg file
    const fileInformation = {
      fileData,
      fileObject,
    }
    return uploadFileRequest(fileInformation);
  })
  .then(({ identifier }) => {
    dispatch(fileUploadSuccess(id, identifier));
  })
  .catch(({ error }) => {
    if (retry && retry >= MAX_RETRY) {
      dispatch(fileUploadFailure(id, error));
    }
    else {
      const nextRetry = retry === undefined ? 1 : retry + 1;
      dispatch(fileUploadRetry(id, nextRetry));
    }
    return Promise.reject(error);
  });
};

// Upload all files from files object array
const uploadFiles = (fileIds) => (dispatch, getState, { ElectronHelper }) => {

  const state = getState();
  const files = fileListSelector(state).filter(fileObject => fileIds.includes(fileObject.id));
  
  // set status
  files.forEach((file) => {
    dispatch(fileUploadRequest(file.id));
  });

  let delay = DELAY;

  // create promises
  const promises = files.map((fileObject) => {
    return ElectronHelper.sendAsync('read-spr-file', { fileObject }, 0)
    .catch((error) => Promise.reject({
      error,
      type: 'access',
    }))
    .then((fileData) => {
      // upload jpg file
      const fileInformation = {
        fileData,
        fileObject,
      }
      return uploadFileRequest(fileInformation)
    })
    .then(({ imgUrl }) => {
      dispatch(fileUploadSuccess(fileObject.id, imgUrl));
    })
    .catch(({ error }) => {
      if (fileObject.retry && fileObject.retry >= MAX_RETRY) {
        dispatch(fileUploadFailure(fileObject.id, error));
      }
      else {
        const nextRetry = fileObject.retry === undefined ? 1 : fileObject.retry + 1;
        setTimeout(dispatch, delay, fileUploadRetry(fileObject.id, nextRetry));
        delay *= TIMEOUT_STRETCH;
      }
      return Promise.reject(error);
    });
  });

  Promise.all(promises)
  .then(() => {
    console.log('All files uploaded');
  })
  .catch((error) => {
    console.log('got error:', error, files);
  });
};

// ----------------------------------------------------------------------------
// reducers

const requestInitialState = {
  lastSuccess: null,
  lastError: null,
  error: null,
  loading: false,
};

const requestFileStatus = (state = requestInitialState, action) => {
  switch (action.type) {
    case FILES_STATUS_REQUEST:
      return {
        ...requestInitialState,
        loading: true,
      };
    case FILES_STATUS_SUCCESS:
      return {
        ...state,
        loading: false,
        lastSuccess: Date.now(),
      };
    case FILES_STATUS_FAILURE:
      return {
        ...state,
        loading: false,
        lastError: Date.now(),
        error: action.payload.error,
      };
    default:
      return state;
  }
};

const requestFileUpload = (state = requestInitialState, action) => {
  switch (action.type) {
    case FILE_UPLOAD_REQUEST:
      return {
        ...requestInitialState,
        loading: true,
      };
    case FILE_UPLOAD_SUCCESS:
      return {
        ...state,
        loading: false,
        lastSuccess: Date.now(),
      };
    case FILE_UPLOAD_FAILURE:
      return {
        ...state,
        loading: false,
        lastError: Date.now(),
        error: action.payload.error,
      };
    default:
      return state;
  }
};

//-------------------------------------------------------------------------------------------------
const byId = (state = {}, action) => {
  switch (action.type) {
    case FILE_UPLOAD_REQUEST:
      return {
        ...state,
        [action.payload.fileId]: {
          ...state[action.payload.fileId],
          status: RequestStatus.PENDING,
        },
      };
    case FILE_UPLOAD_SUCCESS:
      return {
        ...state,
        [action.payload.fileId]: {
          ...state[action.payload.fileId],
          identifier: action.payload.identifier,
          status: RequestStatus.UPLOADED,
        },
      };
    case FILE_UPLOAD_FAILURE:
      return {
        ...state,
        [action.payload.fileId]: {
          ...state[action.payload.fileId],
          status: RequestStatus.FAILURE,
        },
      };
    case FILE_UPLOAD_RETRY:
      return {
        ...state,
        [action.payload.fileId]: {
          ...state[action.payload.fileId],
          status: RequestStatus.RETRY,
          retry: action.payload.retry,
        }
      };
    case FILES_STATUS_SUCCESS:
      return {
        ...state,
        ...action.payload.byId,
      }
    default:
      return state;
  }
}

const ids = (state = [], action) => {
  switch (action.type) {
    case FILES_STATUS_SUCCESS:
      return [...new Set([...state, ...action.payload.ids])]
    default:
      return state
  }
};

// export default (state = {}, action) => {
//   const { payload } = action
//   switch (action.type) {
//     case FILE_SET_VALUE:
//       return {
//         ...state,
//         [payload.key]: [payload.value],
//       }
//     case FILE_SET_VALUES:
//       return {
//         ...state,
//         ...payload,
//       }
//     case FILE_CLEAR_VALUE:
//       return {
//         ...state,
//         [payload]: undefined,
//       }
//     default:
//       return state
//   }
// }

export {
  uploadFile,
  uploadFiles,
  filesStatusRequest,
  filesStatusSuccess,
  filesStatusFailure,
};

const filesReducer = combineReducers({
  requestFileStatus,
  requestFileUpload,
  byId,
  ids,
});

export default (state, action) => {
  if (action.type === FILES_STATUS_REQUEST) {
    state = undefined;
  }
  return filesReducer(state, action);
}
