import { createSelector } from 'reselect';

import { INITIAL, READY, RETRY, DONE, FAILURE, PENDING, MISSING, UPLOADED, SUCCESS } from '../../util/RequestStatus';

const filesStateSelector = state => state.files;

export const fileIdsSelector = createSelector(
  filesStateSelector,
  ({ ids }) => ids,
);

export const fileByIdSelector = createSelector(
  filesStateSelector,
  ({ byId }) => byId,
);

export const fileRequestSelector = createSelector(
  filesStateSelector,
  ({ requestFileStatus }) => requestFileStatus,
);

export const fileUploadSelector = createSelector(
  filesStateSelector,
  ({ requestFileUpload }) => requestFileUpload,
);

export const fileListSelector = createSelector(
  fileIdsSelector,
  fileByIdSelector,
  (ids, byId) => {
    if (ids.length === null) {
      return [];
    }
    return ids.map(id => byId[id])
      .filter(item => item !== null)
  },
);

export const fileUploadRetrySelector = createSelector(
  fileIdsSelector,
  fileByIdSelector,
  (ids, byId) => {
    if (ids.length === null) {
      return [];
    }
    return ids.map(id => byId[id])
      .filter(item => item.status === RETRY);
  },
);

export const fileUploadDoneSelector = createSelector(
  fileIdsSelector,
  fileByIdSelector,
  (ids, byId) => {
    if (ids.length === null) {
      return [];
    }
    return ids.map(id => byId[id])
      .filter(item => item.status === DONE);
  },
);

export const fileUploadFailedSelector = createSelector(
  fileIdsSelector,
  fileByIdSelector,
  (ids, byId) => {
    if (ids.length === null) {
      return [];
    }
    return ids.map(id => byId[id])
      .filter(item => item.status === FAILURE);
  },
);

export const fileStatusSelector = createSelector(
  fileIdsSelector,
  fileByIdSelector,
  (ids, byId) => {
    if (ids.length === null) {
      return [];
    }
    const requests = ids.map(id => byId[id].status)
    if (requests.filter(item => item === FAILURE).length > 0) {
      return FAILURE;
    }
    if (requests.filter(item => item === RETRY).length > 0) {
      return RETRY;
    }
    if (requests.filter(item => item === PENDING).length > 0) {
      return PENDING;
    }
    if (requests.filter(item => item === MISSING).length > 0) {
      return MISSING;
    }
    if (requests.filter(item => item === UPLOADED).length > 0) {
      return UPLOADED;
    }
    if (requests.filter(item => item === READY).length > 0) {
      return READY;
    }
    if (requests.filter(item => item === INITIAL).length > 0) {
      return INITIAL;
    }
    if (requests.filter(item => item === SUCCESS).length > 0) {
      return SUCCESS;
    }
    return INITIAL;
  },

);


