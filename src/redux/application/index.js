import nibriam from 'nxc-nibriam';
import { combineReducers } from 'redux';

export const APP_IDLE = 'APP_IDLE';

// action types
const APPLICATION_WILL_LOAD = 'APPLICATION_WILL_LOAD';
const APPLICATION_DID_LOAD = 'APPLICATION_DID_LOAD';

const APPLICATION_USER_SET = 'APPLICATION_USER_SET';

const APPLICATION_LOGIN_SET_STATUS = 'APPLICATION_LOGIN_SET_STATUS';

// simple actions
const applicationWillLoad = () => ({ type: APPLICATION_WILL_LOAD });
const applicationDidLoad = () => ({ type: APPLICATION_DID_LOAD });

const applicationUserSet = (user) => ({
  type: APPLICATION_USER_SET,
  user,
});

const applicationLoginSetStatus = status => ({
  type: APPLICATION_LOGIN_SET_STATUS,
  payload: status,
});

// complex actions
const loginUser = credentials => async (dispatch, _ , { ElectronHelper }) => {
  const ipcRenderer = ElectronHelper.getIpcRenderer();
  // send credentials to electron backend
  ipcRenderer.send('auth', credentials);

  // try to login again with nibriam
  try {
    const user = await nibriam();

    // save user
    dispatch(applicationUserSet(user));

    // clean up login status
    dispatch(applicationLoginSetStatus({
      loggedIn: true,
      message: '',
    }))
  } catch (e) {
    dispatch(applicationLoginSetStatus({
      loggedIn: false,
      message: 'Invalid credentials, please update credentials',
    }));
  }
};

const loadApplication = () => {
  return (async (dispatch) => {
    dispatch(applicationWillLoad());

    try {
      const user = await nibriam()
      dispatch(applicationUserSet(user));
    } catch(e) {
      console.log('Could not load user, navigate to login page');
    }

    // app is loaded
    dispatch(applicationDidLoad());
  });
};

// reducers
const status = (state = null, action) => {
  switch (action.type) {
    case APPLICATION_WILL_LOAD:
    case APPLICATION_DID_LOAD:
      return action.type;
    default:
      return state;
  }
};

const user = (state = { NIBRFirst: null }, action) => {
  switch (action.type) {
    case APPLICATION_USER_SET:
      return action.user;
    default:
      return state;
  }
};

const loginStatus = (state = {
  loggedIn: false,
  message: '',
}, action) => {
  switch (action.type) {
    case APPLICATION_LOGIN_SET_STATUS:
      return action.payload;
    default:
      return state;
  }
};

export {
  loadApplication,
  loginUser,
};

export default combineReducers({
  status,
  user,
  loginStatus,
});
