import { createSelector } from 'reselect';

export const applicationTimeSelector = state => state.applicationTime;

export const applicationStateSelector = state => state.application;

export const applicationUserSelector = createSelector(
  applicationStateSelector,
  ({ user }) => user,
);

export const applicationStatusSelector = createSelector(
  applicationStateSelector,
  ({ status }) => status,
);

export const applicationUserLoginSelector = createSelector(
  applicationStateSelector,
  ({ loginStatus }) => loginStatus,
);

export const applicationUserIsAuthenticatedSelector = createSelector(
  applicationStateSelector,
  ({ user }) => user.NIBR521 !== undefined,
);
