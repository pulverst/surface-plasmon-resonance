import { combineReducers } from 'redux';

import applicationReducer from './application';
import compoundsReducer from './compounds';
import filesReducer from './files';
import metaReducer from './meta';

const reducers = combineReducers({
  appTime: Date.now,
  application: applicationReducer,
  compounds: compoundsReducer,
  files: filesReducer,
  meta: metaReducer,
})

const rootReducer = (state, action) => {
  // clean state and logout
  if (action.type === 'USER_LOGOUT') {
    state = undefined
  }
  return reducers(state, action)
}
export default rootReducer;
