import { createSelector } from 'reselect';
import { fileUploadFailedSelector } from '../files/selector';
import { compoundUploadFailedSelector } from '../compounds/selector';

const fileUploadFailedReactor = createSelector(
  fileUploadFailedSelector,
  (ids) => {
    console.log('fileUploadFailedReactor', ids);
    return null;
  }
);

const compoundUploadFailedReactor = createSelector(
  compoundUploadFailedSelector,
  (ids) => {
    console.log('compoundUploadFailedReactor', ids);
    return null;
  }
);

export default [
  fileUploadFailedReactor,
  compoundUploadFailedReactor,
];