import { connect } from 'react-redux';

import { applicationUserSelector } from '../redux/application/selectors';
import { compoundListSelector, compoundStatusSelector } from '../redux/compounds/selector';
import { metaStateSelector } from '../redux/meta/selector';
import { uploadCompounds } from '../redux/compounds';
import { fileByIdSelector, fileStatusSelector } from '../redux/files/selector';

import SprDataView from './SprDataView';

const select = state => ({
  user: applicationUserSelector(state),
  compounds: compoundListSelector(state),
  files: fileByIdSelector(state),
  compoundStatus: compoundStatusSelector(state),
  fileStatus: fileStatusSelector(state),
  meta: metaStateSelector(state),
});

const mapDispatchToProps = ({
  uploadCompounds,
});

export default connect(select, mapDispatchToProps)(SprDataView);
