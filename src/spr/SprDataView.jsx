import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { LoadingIndicator } from '@nibr-ux/loading';
import { Colors } from '../styles';
import { Button } from 'reactstrap';
import { Table } from 'reactstrap';
import Card from '../ui/Card';
//import { List, ListItem } from '../ui/List';
import { INITIAL, FAILURE, PENDING, MISSING, UPLOADED,VALIDATED, READY } from '../util/RequestStatus';


import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faFileImage, faTimesCircle } from '@fortawesome/pro-light-svg-icons';
import { faCheck } from '@fortawesome/pro-light-svg-icons';

const getStatusColor = compound => {
  switch (compound.status) {
    case INITIAL:
      return Colors.Black.Default;
    case VALIDATED:
      return Colors.Green.Default;
    case FAILURE:
      return Colors.Red.Default;
    case UPLOADED:
      return Colors.Blue.Default;
    default:
      return '#494949';
  }
};

const getFileColor = file => {
  switch (file.status) {
    case READY:
    case PENDING:
      return Colors.Green.Default;
    case FAILURE:
    case MISSING:
      return Colors.Red.Default;
    case UPLOADED:
      return Colors.Blue.Default;
    default:
      return Colors.Black.Default;
  }
}

// const StyledCompound = styled(ListItem)`
//   color: ${compound => getStatusColor(compound)};
// `;

const StyledCompound = styled.td`
  color: ${compound => getStatusColor(compound)};
`;

const StyledFile = styled.div`
  color: ${file => getFileColor(file)};
`;

const StyledButtonArea = styled.div`
  text-align: center;
  margin: 20px;
`;

class SprDataView extends Component {
  startUpload = this.startUpload.bind(this);

  startUpload() {
    const { compounds, uploadCompounds } = this.props;
    uploadCompounds(compounds);
  }

  statusIndicator(status) {
    switch(status) {
      case PENDING: return <LoadingIndicator/>
      case UPLOADED: return <FontAwesomeIcon icon={faCheck} /> 
      default: return null;
    }
  }

  render() {
    const { compounds, meta, files, compoundStatus, fileStatus } = this.props;
    const { loading } = true;

    return (
      <div className="spr-view">
        { meta && (
          <div>
            <Card>
              <div>
              {meta['SPR Method']} {meta['Project Code']}<br/>
              Source:{meta['Source']}<br/>
              Target:{meta['Target']}
              </div>
            </Card>
            <Card>
              <Table>
                <tbody>
                  {compounds.map((compound, i) => {
                    return (
                      <tr>
                      <StyledCompound key={i} { ...this.props.compounds[i] }>
                        { compound['Compound ID'] }
                      </StyledCompound>
                      <td>
                        {this.statusIndicator(compound.status)}
                      </td>
                      {compound.files.map((file,j) => {
                        return file !== null ?
                        (
                          <td> 
                            <StyledFile { ...{status: files[file].status} }>
                              { files[file].status === MISSING ? (
                                <FontAwesomeIcon icon={faTimesCircle} />
                              ):(
                                <FontAwesomeIcon icon={faFileImage} />
                              )}
                            </StyledFile>
                          </td>
                        ) : <td></td>
                      })}           
                      </tr>
                    )
                  })}
                </tbody>
              </Table>
            </Card>
          </div>
        )}
        <StyledButtonArea>
          {fileStatus === READY & compoundStatus === VALIDATED & !loading ? (
            <Button color="primary" onClick={this.startUpload}>Upload compounds</Button>
          ):(
            <Button color="secondary" onClick={this.startUpload} disabled>Upload compounds</Button>
          )}
        </StyledButtonArea>
      </div>
    )
  }
}

SprDataView.propTypes = {
  user: PropTypes.shape({
    NIBRFull: PropTypes.string,
    NIBR521: PropTypes.string,
  }),
  meta: PropTypes.arrayOf(
    PropTypes.shape({})
  ).isRequired,
  compounds: PropTypes.arrayOf(
    PropTypes.shape({})
  ).isRequired,
  files: PropTypes.arrayOf(
    PropTypes.shape({})
  ).isRequired,
  compoundStatus: PropTypes.string.isRequired,
  fileStatus: PropTypes.func.isRequired,
  uploadCompounds: PropTypes.func.isRequired,
}

SprDataView.defaultProps = {
  user: null,
}

export default SprDataView
