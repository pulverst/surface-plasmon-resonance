import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withRouter, Switch, Route } from 'react-router-dom';
import styled from 'styled-components';
 
import NxFooter from 'nxc-core/NxFooter';
import NxPrivacy from 'nxc-core/NxPrivacy';
 
import Header from './ui/Header';
import NotFound from './ui/NotFound';
 
import * as Paths from './paths';
 
import Login from './login/LoginContainer';
import Home from './home/HomeContainer';
import Spr from './spr/SprDataContainer';

import HomeContainer from './home/HomeContainer';
 
import PrivateRoute from './util/PrivateRoute';

import { reportReactError, createUnknownError } from './util/ErrorHandler';
import { loadApplication } from './redux/application';
 
const StyledApplicationWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: stretch;
  flex-direction: column;
  height: 100%;
`;
 
const StyledApplicationContent = styled.main`
  flex-grow: 1;
  flex-shrink: 1;
`;
 
class App extends Component {
  static propTypes = {
    loadApplication: PropTypes.func.isRequired,
    reportReactError: PropTypes.func.isRequired,
  }
 
  componentWillMount() {
    this.props.loadApplication();
  }
 
  componentDidCatch(error, info) {
    // handle error on root level
    this.props.reportReactError(createUnknownError(error, info));
  }
 
  render() {
    const {
      REACT_APP_WEBTRENDS_ID,
      REACT_APP_APPSTORE_ID,
      REACT_APP_VERSION,
    } = process.env;
 
    const showPrivacyNote = REACT_APP_WEBTRENDS_ID && REACT_APP_APPSTORE_ID;
 
    return (
      <StyledApplicationWrapper>
        <Header />
        <StyledApplicationContent role="main">
          <Switch>
            <PrivateRoute exact path={Paths.HOME} component={Home} />
            <PrivateRoute exact path={Paths.SPR} component={Spr} />
            <Route exact path={Paths.LOGIN} component={Login} />
            <Route component={NotFound} />
            <Route exact path={Paths.HOME} component={HomeContainer} />
            <Route path="*" component={NotFound} />
          </Switch>
        </StyledApplicationContent>
        <NxFooter version={ REACT_APP_VERSION }>
          {showPrivacyNote && <NxPrivacy />}
        </NxFooter>
      </StyledApplicationWrapper>
    );
  }
};
 
const select = state => {
  console.log(state)
  return {};
};
// const select = () => ({})
 
const mapDispatchToProps = ({
  reportReactError,
  loadApplication,
});
 
export default withRouter(connect(select, mapDispatchToProps)(App));
 