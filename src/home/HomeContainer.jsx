import { connect } from 'react-redux';

import { applicationUserSelector } from '../redux/application/selectors';

import { loadExportData } from '../redux/compounds';
// import { fileListSelector, fileRequestSelector } from '../redux/files/selector';

import HomeView from './HomeView';

const select = state => ({
  user: applicationUserSelector(state)
});

const mapDispatchToProps = ({
  loadExportData,
});

export default connect(select, mapDispatchToProps)(HomeView);
