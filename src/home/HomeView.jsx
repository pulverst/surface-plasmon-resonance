import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';
import { Jumbotron, Button } from 'reactstrap';
import * as Paths from '../paths';

const { dialog } = window.require('electron').remote;

const StyledJumbotron = styled(Jumbotron)`
  width: 80%;
  margin: 4rem auto;
`;

const StyledButtonArea = styled.div`
  text-align: center;
  margin: 20px;
`;
 
class HomeView extends Component {

  loadSprFile = this.loadSprFile.bind(this)

  loadSprFile() {
    const fileList = dialog.showOpenDialog({
      title: 'Select SPR file',
      multiSelections: true,
    })
    if(fileList){
      fileList.forEach(filename => {
        this.props.loadExportData(filename)
      })

      // navigate to files view
      this.props.history.push(Paths.SPR)
    }
  }

  render() {
    const { user } = this.props;
    return (
      <div>
        <StyledJumbotron>
          <h1 className="display-3">
            Hello,
            {' '}
            {user ? user.NIBRFirst : 'there...'}
            !
          </h1>
          <p className="lead">
            {user
              ? 'We successfully received your NIBR IAM information'
              : (
                <span>
                  ...we&amp;re still trying to identify you
                </span>
              )}
          </p>
          <p>
            {user && (<code>{JSON.stringify(user, null, 2)}</code>)}
          </p>
          <hr className="my-2" />
          <p>
            Welcome to the
            {' '}
            <strong>new version</strong>
            {' '}
  of NIBR Web App Generator. It allows you to quickly create web apps based on a
  carefully-selected set of libraries and frameworks.
          </p>
        </StyledJumbotron>
        <StyledButtonArea>
          <Button color="primary" onClick={this.loadSprFile}>Load SPR export</Button> <br />
        </StyledButtonArea>
      </div>
    );
  }
}
 
HomeView.propTypes = {
  user: PropTypes.shape({
    NIBRFull: PropTypes.string,
    NIBR521: PropTypes.string,
  }),
  files: PropTypes.arrayOf(
    PropTypes.shape({})
  ).isRequired,
  fileRequest: PropTypes.shape({
    loading: PropTypes.bool.isRequired,
  }).isRequired,
  loadExportData: PropTypes.func.isRequired,
};
 
HomeView.defaultProps = {
  user: null,
};
 
export default HomeView;