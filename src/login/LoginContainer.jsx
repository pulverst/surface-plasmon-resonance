import { connect } from 'react-redux';

import { loginUser } from '../redux/application';
import { applicationUserLoginSelector, applicationUserIsAuthenticatedSelector } from '../redux/application/selectors';

import Login from './Login';

const select = state => ({
  isAuthenticated: applicationUserIsAuthenticatedSelector(state),
  message: applicationUserLoginSelector(state).message,
});

const mapDispatchToProps = ({
  loginUser,
});

export default connect(select, mapDispatchToProps)(Login);
