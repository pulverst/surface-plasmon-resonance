import React, { Component } from 'react';
import PropTypes from 'prop-types';

import {
  Form,
  FormGroup,
  Label,
  Container,
  Row,
  Col,
  Button,
  Alert,
} from 'reactstrap'

import Input from '../ui/Form/Input'
import { generateFormState } from '../ui/Form/formUtils';

import * as Paths from '../paths';

const FormDefinition = {
  USERNAME: 'user521',
  PASSWORD: 'password',
}

const initialState = {
  valid: false,
  dirty: false,
  form: {},
}

class Login extends Component {
  static propTypes = {
    data: PropTypes.shape({}),
    message: PropTypes.string.isRequired,
    isAuthenticated: PropTypes.bool.isRequired,
  }

  static defaultProps = {
    data: {},
    isAuthenticated: false,
  }

  state = initialState

  onInputChange = this.onInputChange.bind(this)
  onClickCancel = this.onClickCancel.bind(this)
  onClickSubmit = this.onClickSubmit.bind(this)
  onKeyPress    = this.onKeyPress.bind(this)

  componentWillMount() {
    const { isAuthenticated } = this.props;
    console.log(this.props);

     // navigate to index if authenticated
    if (isAuthenticated) {
      this.props.history.push(Paths.HOME);
      return;
    }

    this.setState({
      ...generateFormState(FormDefinition, this.props.data),
    })
  }

  componentWillReceiveProps(newProps) {
    const { isAuthenticated } = newProps;
    console.log(this.props);
    if (isAuthenticated) {
      this.props.history.push(Paths.HOME);
    }
  }

  getFormState(key) {
    return this.state.form[key] || null;
  }

  getFormValue(key) {
    const data = this.getFormState(key);
    return data ? data.value : '';
  }

  getFormValidation(key) {
    const data = this.getFormState(key);
    return data ? data.validation : undefined;
  }

  onInputChange(key) {
    return (e, obj) => {

      // get state validation
      const stateValidation = Object.values(Form)
        .filter(validationKey => validationKey !== key) // don't validate current input value here
        .map((validationKey) => {
          const validation = this.getFormValidation(validationKey);
          
          return {
            key: validationKey,
            valid: validation ? validation.valid : false,
          };
        });
      
      // create validation result array (boolean array)
      const validation = [
        ...stateValidation,
        { key, valid: obj.validation.valid }
      ]
      // check if all values equals true
      const valid = validation.reduce((prev, cur) => prev && cur.valid, true);

      this.setState({
        dirty: true,
        valid,
        form: {
          ...this.state.form,
          [key]: {...obj},
        },
      });
    };
  }

  onClickCancel() {
    this.setState({
      ...generateFormState(FormDefinition, this.props.data),
    });
  }

  onClickSubmit() {
    this.loginUser();
  }

  onKeyPress(e) {
    if(e.key === 'Enter'){
      this.loginUser();
    }
  }

  loginUser() {
    const credentials = {
      username: this.getFormValue(FormDefinition.USERNAME),
      password: this.getFormValue(FormDefinition.PASSWORD),
    };
    this.props.loginUser(credentials);
  }

  renderMessage() {
    const { message } = this.props;

    if (message === '') {
      return null;
    }

    return (
      <Row>
        <Col>
          <Alert color="danger">
            {message}
          </Alert>
        </Col>
      </Row>
    )
  }

  render() {
    return (
      <Container className="login-view">
        <Row>
          <Col>
            <h1>Please add your credentials</h1>
          </Col>
        </Row>
        {this.renderMessage()}
        <Row>
          <Col>
            <Form>
              <FormGroup>
                <Label for={FormDefinition.USERNAME}>Your User521</Label>
                <Input
                  type="text"
                  onChange={this.onInputChange(FormDefinition.USERNAME)}
                  value={this.getFormValue(FormDefinition.USERNAME)}
                  validation={this.getFormValidation(FormDefinition.USERNAME)}
                  rules={[]}
                  inputProps={{
                    placeholder: 'user521',
                  }}
                />
              </FormGroup>
              <FormGroup>
                <Label for={FormDefinition.PASSWORD}>Your password</Label>
                <Input
                  type="password"
                  // onKeyDown={() => console.log('onKeyDown')} //this.onKeyPress}
                  // onKeyPress={console.log('onKeyPress')} //this.onKeyPress}
                  // onKeyUp={console.log('onKeyUp')} //this.onKeyPress}
                  onChange={this.onInputChange(FormDefinition.PASSWORD)}
                  value={this.getFormValue(FormDefinition.PASSWORD)}
                  validation={this.getFormValidation(FormDefinition.PASSWORD)}
                  rules={[]}
                  inputProps={{
                    placeholder: 'Your password',
                  }}
                />
              </FormGroup>
              <div className="controls">
                <Button
                  type="button"
                  color="default"
                  onClick={this.onClickCancel}
                >Cancel</Button>
                <Button
                  type="button"
                  color="primary"
                  onClick={this.onClickSubmit}
                >Submit</Button>
              </div>
            </Form>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default Login
