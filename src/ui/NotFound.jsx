import React, { Component } from 'react';
import PropTypes from 'prop-types';
import styled from 'styled-components';

import { colors } from '@nibr-ux/style';
 
import * as Paths from '../paths';

const StyledNotFound = styled.div`
  margin-top: 2rem;
`;
 
const StyledHeader = styled.h1`
  width: 80%;
  margin: 0 auto;
  font-size: 2.5rem;
`;
 
const StyledCode = styled.code`
  color: ${colors.red};
  padding: 0 0.25rem;
`;
 
// const NotFound = ({ location }) => (
class NotFound extends Component {
  componentDidMount() {
    console.log(this.props);

    const { location: { pathname }, history } = this.props;
    if (pathname.toLowerCase().indexOf('resources/app') >= 0) {
      console.log('forward to index');
      // fallback for app start
      history.push(Paths.HOME);
    } else {
      console.log('not forward to index');
    }
  }

  render() {
    return (
      <StyledNotFound>
        <StyledHeader>
          <strong>Whhooppss</strong>
          {' '}
    it seems there is no page for route:
          <StyledCode>{this.props.location.pathname}</StyledCode>
        </StyledHeader>
      </StyledNotFound>
    );
  }
}
 
NotFound.propTypes = {
  location: PropTypes.shape({
    pathname: PropTypes.string.isRequired,
  }).isRequired,
};
 
export default NotFound;
 