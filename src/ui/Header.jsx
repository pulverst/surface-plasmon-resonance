import React, { Component } from 'react';
import PropTypes from 'prop-types';

import NxHeader from 'nxc-core/NxHeader';
import NxGetHelp from 'nxc-core/NxGetHelp';

import * as Paths from '../paths';

const barLinks = [
  {
    label: 'Home',
    path: Paths.HOME,
  }
];

const appLinks = [
  {
      url: 'http://go/ui',
      name: 'FAQs',
  },
  {
      url: 'http://go/ui',
      name: 'Documentation',
  },
  {
      url: 'http://go/bootstrap',
      name: 'Bootstrap',
  },
];

const titleHref = '/'
const initialState = {
  activePath: '',
};

class Header extends Component {
  static propTypes = {
    appName: PropTypes.string.isRequired,
    appDescription: PropTypes.string.isRequired,
    jiraProjectCode: PropTypes.string.isRequired,
  }

  static defaultProps = {
    appName: process.env.REACT_APP_NAME,
    appDescription: process.env.REACT_APP_DESCRIPTION,
    jiraProjectCode: process.env.REACT_APP_JIRA_PROJECT_CODE,
  }

  state = initialState

  onTitleClick = this.onTitleClick.bind(this)

  componentWillMount() {
    const { location: { pathname } } = window
    this.setState({
      activePath: pathname ? `/${pathname.split('/')[1]}` : ''
    })
  }

  onTitleClick() {
    this.setState({
      activePath: titleHref,
    });
  }

  render() {
    const { activePath } = this.state;
    const { appName, appDescription, jiraProjectCode } = this.props;

    const nodeEnv = process.env.NODE_ENV;

    console.log(nodeEnv, appName)
    const jiraOptions = {
      issueTypeName: 'Bug',
      defaultAssignee: 'your-user-id',
      projectCode: jiraProjectCode,
      component: 'Feedback/Suggestion',
    };

    const helpContent = (
      <NxGetHelp
        appName={appName}
        appLinks={appLinks}
        deployState={nodeEnv === 'production' ? 'prod' : 'dev'}
        jiraOptions={jiraOptions}
      />
    );

    return (
      <NxHeader
        appName={appName}
        description={appDescription}
        deployState={nodeEnv !== 'production' ? nodeEnv.substring(0, 3) : ''}
        titleHref={titleHref}
        deployHref={null}
        navBarInfo={{ activePath, barLinks }}
        helpTrayContent={helpContent}
        onTitleClick={this.onTitleClick}
        displaySearch={false}
        appsTrayContent={null}
        notificationsTrayContent={null}
        hasUnreadNotifications={false}
        onNotificationsTrayOpen={() => null}
      />
    )
  }
}

export default Header
