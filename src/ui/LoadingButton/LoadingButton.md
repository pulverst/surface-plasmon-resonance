# Loading Button

Easy to use loading button to visualize fetch status of running network call

## Properties
| propName      | propType                  | defaultValue  | required      |
|---------------|:--------------------------|:--------------|:--------------|
| children      | node                      | -             | x             |
| loading       | boolean                   | -             |               |
| className     | string                    | undefined     |               |
| position      | string ('left', 'right')  | 'left'        |               |