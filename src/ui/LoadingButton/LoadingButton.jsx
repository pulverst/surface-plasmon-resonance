import React from 'react';
import PropTypes from 'prop-types';

import NxLoadingIndicator from 'nxc-core/NxLoadingIndicator';

const LoadingButton = (props) => {
  const { className, position, loading } = props;

  const buttonClassName = `btn loading-button ${position}`;
  const newClassName = className ? `${buttonClassName} ${className}` : buttonClassName;

  const newProps = {
    ...props,
    className: newClassName,
    // reset unnused props
    children: undefined,
    position: undefined,
    loading: undefined,
  };

  const loadingIndicator = 
    loading && <span className="indicator"><NxLoadingIndicator size="small" /></span>

  return (
    <button type="button" {...newProps}>
      {loadingIndicator}
      <span>{props.children}</span>
    </button>
  )
}

LoadingButton.propTypes = {
  loading: PropTypes.bool,
  position: PropTypes.oneOf(['left', 'right']),
  className: PropTypes.string,
  children: PropTypes.node.isRequired,
}

LoadingButton.defaultProps = {
  loading: false,
  position: 'right',
  className: undefined,
}

export default LoadingButton
