export const generateFormState = (definition, data = {}) => {
  const formComponents = Object.values(definition)
    .map((key) => {
      const value = data[key];
      return {
        key,
        obj: {
          validation: {
            dirty: false,
            valid: value ? true : false,
            errors: [],
          },
          value,
        },
      }
    })
    .reduce((prev, cur) => ({
      ...prev,
      [cur.key]: cur.obj,
    }), {});

  return {
    valid: true, // simple case: we assume injected data is always valid, improve this if needed
    dirty: false,
    form: formComponents,
  };
};
