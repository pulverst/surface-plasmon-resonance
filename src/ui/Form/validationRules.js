// validation rules
export const minLength = min => (value) => {
  const msg = `Please enter (min ${min} letters)`;

  if (!value) {
    return msg;
  }

  if (value.length < min) {
    return msg;
  }

  return null;
};

export const maxLength = max => (value) => {
  const msg = `Please enter (max ${max} letters)`;

  if (!value) {
    return msg;
  }

  if (value.length > max) {
    return msg;
  }

  return null;
};