import React, { Component } from 'react';
import PropTypes from 'prop-types';
import uuid from 'uuid';

import { Input } from 'reactstrap';

const initialState = {
  active: false,
};

export default class InputComponent extends Component {
  static propTypes = {
    value: PropTypes.string.isRequired,
    onChange: PropTypes.func.isRequired,
    id: PropTypes.string,
    inputProps: PropTypes.shape({}),
    validation: PropTypes.shape({
      valid: PropTypes.bool,
      dirty: PropTypes.bool,
      errors: PropTypes.arrayOf(PropTypes.string),
    }),
    rules: PropTypes.arrayOf(PropTypes.func),
  }

  static defaultProps = {
    value: '',
    id: uuid.v4(),  
    inputProps: {
      key: uuid.v4(),
    },
    validation: {
      valid: false,
      dirty: false,
      errors: [],
    },
    rules: [],
  };

  state = initialState;

  onChangeInput = this.onChangeInput.bind(this);
  onBlurInput = this.onBlurInput.bind(this);

  onChangeInput(e) {
    const { onChange, rules } = this.props;
    const { target: { value } } = e;

    const errors = rules
      .map(rule => rule(value))
      .filter(error => error !== null);

    const obj = {
      value,
      validation: {
        valid: errors.length === 0,
        dirty: true,
        errors,
      },
    }

    onChange && onChange(e, obj);
  }

  onBlurInput(e) {
    this.onChangeInput(e);

    // check for disable active
    const { target: { value } } = e

    if(value.length === 0) {
      this.setState({ active: false });
    }
  }

  render() {
    const { id } = this.state;
    const { inputProps, type, value, validation } = this.props;
    
    const valid = !validation.dirty || (validation.valid ? true : false);

    const inputCss = ['input', inputProps.className]
      .filter(item => item !== undefined);

    const props = {
      ...inputProps,
      type,
      className: inputCss.join(' '),
      id,
      value,
      onChange: this.onChangeInput,
      onBlur: this.onBlurInput,
      onClick: this.toggleActive,
      onFocus: this.toggleActive,
      invalid: valid ? false : true,
      valid: valid ? true : false,
    };

    return <Input {...props} />
  }
}