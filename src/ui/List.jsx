import React from 'react';
import styled from 'styled-components';

const StyledListItem = styled.li``;
export const ListItem = props => <StyledListItem {...props} />;


const StyledList = styled.ul``;
export const List = props => <StyledList {...props}/>


export default List;
