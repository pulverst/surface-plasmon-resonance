const parser = require('papaparse');
const uuid = require('uuid');
const path = require('path');
const fs = require('fs');

const { INITIAL, READY, MISSING } = require('./util/RequestStatus');
const { fileIndex, valueIndex, metaIndex } = require('./util/ParserInfo');

exports.readFile = (filename) => {
  return new Promise((resolve, reject) => {
    fs.readFile(filename, 'utf-8', (error, data) => {
      if(error) {
        reject(error);
      }
      resolve(data);
    });
  });
};

exports.parseFile = (file) => new Promise((resolve, reject) => {
  parser.parse(file, {
    delimiter: undefined,	// auto-detect
    newline: undefined,	// auto-detect
    quoteChar: '"',
    escapeChar: '"',
    header: true,
    trimHeaders: true,
    dynamicTyping: false,
    preview: 0,
    encoding: undefined,
    worker: false,
    comments: false,
    step: undefined,
    download: false,
    complete: results => resolve(results),
    error: error => reject(error),
    skipEmptyLines: true,
    chunk: undefined,
    fastMode: undefined,
    beforeFirstChunk: undefined,
    withCredentials: undefined,
    transform: undefined  
  });
});

exports.fileReady = (path) => {
  // full path needed for this check
  try{
    fs.accessSync(path, fs.constants.R_OK);
  }
  catch(error) {
    return false;
  }
  return true;
};

const fileObject = (sprPath, filename, fileID) => {
  const imageDir = 'img';
  const fileExt = '.png';
  const filepath = path.join(sprPath, imageDir, filename + fileExt);
  return {
    "id": fileID,
    "filename": filename,
    "filepath": filepath,
    "status": this.fileReady(filepath) ? READY : MISSING,
  };
};

const compoundObject = (values, files) => {
  return {
    "id": uuid.v4(),
    "status": INITIAL,
    "files": files,
    ...values
  };
};

exports.parsedData = (sprData, sprPath) => {
  const {data, errors, meta} = sprData;

  const files = [];
  const compounds = [];

  if (errors.length === 0) {

    // headers to pick graphic files
    const fileHeaders = fileIndex.map((index) => {
      return meta.fields[index];
    });

    // headers to pick compound data
    const valueHeaders = valueIndex.map((index) => {
      return meta.fields[index];
    });

    for (let i = 0; i < data.length; i++) {
      let row = data[i];

      // file objects
      const fileList = fileHeaders.filter(header => row[header].length > 0).map(header => {return row[header]});
      // create file objects and ID list
      const fileIDs = fileList.reduce((prev, cur) => {
        let fileID = null;
        if(cur.length > 10) {
          fileID = uuid.v4();
          files.push(fileObject(sprPath, cur, fileID));
        }
        return {
          ...prev,
          [cur]: fileID,
        };
      }, {});
      
      let graphs = [];
      // compound objects
      const valueList = valueHeaders.filter(header => row[header].length > 0).reduce((prev, cur) => {
        
        let value
        // value curation
        if(fileHeaders.filter((file) => { return file === cur }).length > 0) {
          // replace the file name with the file ID
          value = fileIDs[row[cur]];
          graphs.push(value);
        } else {
          // use unchanged
          value = row[cur];
        }

        return {
          ...prev,
          [cur]: value,
        };
      }, {});

      // create compound object
      compounds.push(compoundObject(valueList, graphs));
    }
  }

  return{
    files: files,
    compounds: compounds,
  };
};

exports.metaData = (sprData => {

  const {data, errors, meta} = sprData;

  if (errors.length === 0) {

    // headers to pick meta data
    const metaHeaders = metaIndex.map((index) => {
      return meta.fields[index];
    });
  
    const metaData = metaHeaders.reduce((prev, cur) => {
      const value = data[0][cur];

      return {
        ...prev,
        [cur]: value,
      };
    }, {})
    return {
      ...metaData,
      'headers': meta.fields,
    };
  }
  return {};
});
