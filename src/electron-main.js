const electron = require('electron');
// Module to control application life.
const app = electron.app;
// Module to create native browser window.
const BrowserWindow = electron.BrowserWindow;

// get ipcMain process from electron
const ipcMain = electron.ipcMain;

const path = require('path');
const url = require('url');
const fs = require('fs');

const util = require('./util');
const isDev = require('electron-is-dev')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;

// disabled https errors
app.commandLine.appendSwitch("ignore-certificate-errors");

function createWindow() {
    // Create the browser window.
    mainWindow = new BrowserWindow({
      width: 1400,
      height: 1000,
      /**
       * ATTENTION
       * Potential security issue (if CORS / iFrame is used by Developer)
       * 
       * Status on 29. Nov. 2018
       * DataCatalog S3 upload was not CORS enabled / whitelisted for localhost apps
       * In order to proceed with project, webSecurity is disabled until DataCatalog S3 supports
       * CORS for localhost.
       */
      webPreferences: {
        webSecurity: false,
      },
      show: false,
    });

    // and load the index.html of the app.
    // mainWindow.loadURL('http://localhost:3030')
    const startUrl = process.env.ELECTRON_START_URL || url.format({
      pathname: path.join(__dirname, '/../build/index.html'),
      // pathname: 'index.html',
      protocol: 'file:',
      slashes: true
    });
    console.log(startUrl);
    mainWindow.loadURL(startUrl);
    // mainWindow.loadURL(isDev ? 'http://localhost:3030' : startUrl)

    // Open the DevTools.
    if (isDev) {
      mainWindow.webContents.openDevTools();
    }

    // Show window when ready
    mainWindow.once('ready-to-show', () => {
      mainWindow.show();
    });

    // Show window when ready
    mainWindow.once('ready-to-show', () => {
      mainWindow.show();
    });

    // Emitted when the window is closed.
    mainWindow.on('closed', function () {
        // Dereference the window object, usually you would store windows
        // in an array if your app supports multi windows, this is the time
        // when you should delete the corresponding element.
        mainWindow = null;
    });
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit();
    }
});

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (mainWindow === null) {
        createWindow();
    }
});

// CUSTOM CODE
let credentials;

ipcMain.on('auth', (e, auth) => {
    // Mac only: temp credentials storage
    credentials = auth;
});

const LOAD_SPR_EXPORT_REQUEST = 'load-spr-export';
ipcMain.on(LOAD_SPR_EXPORT_REQUEST, (event, action) => {
  console.log('LOAD_SPR_EXPORT_REQUEST: ', action);

  const filename = action.filename;

  if (filename) {
    util.readFile(filename)
      .then(file => util.parseFile(file))
      .then((results) => {
        const sprPath = path.dirname(filename);
        const payload = {
          meta: {
            "sprFileName": path.basename(filename),
            "sprPath": sprPath,
            ...util.metaData(results)
          },
          ...util.parsedData(results, sprPath)
        };
        event.sender.send(LOAD_SPR_EXPORT_REQUEST, { payload: payload, error: false });
      })
      .catch(error => {
        event.sender.send(LOAD_SPR_EXPORT_REQUEST, { payload: error, error: true });
      });

  } else {
    // filename is empty
    event.sender.send(LOAD_SPR_EXPORT_REQUEST, { payload: {}, error: false });
  }
});

const READ_SPR_FILE_REQUEST = 'read-spr-file';
ipcMain.on(READ_SPR_FILE_REQUEST, (event, action) => {
  console.log('READ_SPR_FILE_REQUEST: ', action);

  const { filepath } = action.fileObject;

  fs.readFile(filepath, (error, data) => {
    if(error){
        event.sender.send(READ_SPR_FILE_REQUEST, { payload: error, error: true });
    }
    event.sender.send(READ_SPR_FILE_REQUEST, { payload: data, error: false });
  });

});

app.on('login', (event, webContents, request, authInfo, callback) => {
  // get credentials and add to nibriam callback
  if (credentials) {
    event.preventDefault();
    callback(credentials.username, credentials.password);

    credentials = undefined;
  }
});